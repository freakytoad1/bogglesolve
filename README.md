# Bogglesolve

Go program to solve a provided boggle board of any square size!

## Instructions
1. Run `main.go` this starts an api server running on port `8080`.
2. `POST` to `127.0.0.1:8080/boggle` with the board as a single string JSON body in the `board` field. Square # of chars.
3. Server attempts to solve board and returns the words found.


### Example

```
Method: POST
Endpoint /boggle
Body:
{
    "board": "abcdefghijklmnop"
}

Return:
<array of answers in string>
```

## Notes
- Unit tests are written for a lot of functions. These show `POSITIVE` results showcasing good expected input and output to a function. Some tests also have `NEGATIVE` results that showcase bad input and output.
- The boggle function is recursive and concurrent. It starts searching for words at each index of the board, each in a go routine. This is a fast way to search as it would scale with the size of the board.
- Answers are saved to a shared answer field in the `solve` struct. This is done using a `sync.Mutex` which prevents threads from stepping on each other.
- Wordlist used is from here: https://www-personal.umich.edu/~jlawler/wordlist.html - this was just a site found online with a decent sized list of english words. Any words with non-alphabetic characters will be filtered out.
- A trie is used to store and search words in the wordlist. This is extremely fast (~1ms vs. 300-400ms for slice).

## Future improvements
- Simple GUI to allow someone to type in their board from a browser.
- Have different backend wordlists.