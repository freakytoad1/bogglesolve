package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/freakytoad1/bogglesolve/boggle"
)

type server struct {
	boggle *boggle.Client
}

func Start() error {
	// set up boggle client
	boggleClient, err := boggle.New()
	if err != nil {
		return fmt.Errorf("unable to create boggle client: %w", err)
	}

	s := &server{
		boggle: boggleClient,
	}

	// set up boggle route
	r := gin.Default()
	r.POST("/boggle", s.handleBoggle)

	// run the server
	if err := r.Run(); err != nil { // listen and serve on 127.0.0.1:8080
		return err
	}

	return nil
}

// handleBoggle takes a POST request with the board as a JSON body in the 'board' field
// attempts to solve the board
func (s *server) handleBoggle(c *gin.Context) {
	input := &struct {
		Board string `binding:"required"`
	}{}

	// validate the json contains the board
	if err := c.ShouldBindJSON(input); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("board input was not provided correctly: %v", err))
		return
	}

	log.Printf("board: %q", input.Board)

	// solve a boggle board
	answers, err := s.boggle.SolveBoggle(input.Board)
	if err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("unable to solve boggle board: %v", err))
		return
	}

	c.JSON(http.StatusOK, answers)
}
