package boggle

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/freakytoad1/bogglesolve/trie"
)

// package boggle solves a 4x4 boggle puzzle and returns all words from the provided 'wordlist' that match

// Rules
// 1. Words must be at least three letters in length.
// 2. Each letter must be a horizontal, vertical, or diagonal neighbor of the one before it.
// 3. No individual letter cube may be used more than once in a word.
// 4. No capitalized words or hyphenated words are allowed.

type Solve struct {
	WordList   *trie.Trie
	board      *board
	answers    map[string]string
	answerLock sync.Mutex
}

// SolveBoggle takes an input board, validates it, and attempts to solve it against the wordlist.
func (c *Client) SolveBoggle(inputBoard string) ([]string, error) {
	// create board
	gameBoard, err := newBoard(inputBoard)
	if err != nil {
		return nil, err
	}

	solver := &Solve{
		WordList: c.WordList,
		board:    gameBoard,
		answers:  make(map[string]string), // initialize map
	}

	// start searching at each board index
	var wg sync.WaitGroup
	for i, row := range solver.board.pieces {
		for j := range row {
			i := i
			j := j
			wg.Add(1)
			go func() {
				defer wg.Done()

				// get answers starting at this index
				solver.solveBoggle("", i, j, []int{})
				log.Printf("[END] end of %q run", solver.board.pieces[i][j].character)
			}()
		}
	}

	// wait for all go routines to finish
	wg.Wait()

	answers := solver.getAnswers()

	if len(answers) == 0 {
		return nil, fmt.Errorf("no boggle answers found")
	}

	return answers, nil
}

// wordAnswer contains if the specified word is an answer or is a future answer
type wordAnswer struct {
	isAnswer       bool
	isFutureAnswer bool
}

// isAnswerOrFuture determines if the 'word' is part of the 'wordlist', and if it is the start of a larger word in the 'wordlist'.
func (c *Solve) isAnswerOrFuture(word string) wordAnswer {
	response := wordAnswer{}

	// is word in list?
	result := c.WordList.Search(word)
	if result.Match {
		// word is in list
		response.isAnswer = true
		log.Printf("[ANSWER] answer found: %s", word)
	}

	// is word the start of a larger word?
	if result.FutureMatch {
		// a future, larger word is possible
		response.isFutureAnswer = true
	}

	return response
}

// pieceUsed determines if the specified piece id has already been used in the answer
// true == used, false == unused
func pieceUsed(pieceID int, usedPieces []int) bool {
	for _, usedID := range usedPieces {
		if pieceID == usedID {
			return true
		}
	}

	return false
}

// solveBoggle is a recursive function that finds all possible words starting at the provided index.
func (c *Solve) solveBoggle(currentWord string, row, column int, usedPieces []int) {
	// add to the currentWord
	currentPiece := c.board.pieces[row][column]
	currentWord = currentWord + currentPiece.character
	usedPieces = append(usedPieces, currentPiece.id)

	// determine if this is an answer or the start of an answer
	answer := c.isAnswerOrFuture(currentWord)
	if answer.isAnswer {
		c.addAnswer(currentWord)
	}
	if !answer.isFutureAnswer {
		// path ends
		return
	}

	// determine next piece of board to look at and if that piece has already been used as part of the answer
	// Rule 2. Each letter must be a horizontal, vertical, or diagonal neighbor of the one before it.
	// Rule 3. No individual letter cube may be used more than once in a word.
	// up
	// row - 1, column same
	upRow := row - 1
	if upRow >= 0 && !pieceUsed(c.board.pieces[upRow][column].id, usedPieces) {
		c.solveBoggle(currentWord, upRow, column, usedPieces)
	}

	// down
	// row + 1, column same
	downRow := row + 1
	if downRow <= c.board.numRowsCols-1 && !pieceUsed(c.board.pieces[downRow][column].id, usedPieces) {
		c.solveBoggle(currentWord, downRow, column, usedPieces)
	}

	// left
	// row same, column - 1
	leftColumn := column - 1
	if leftColumn >= 0 && !pieceUsed(c.board.pieces[row][leftColumn].id, usedPieces) {
		c.solveBoggle(currentWord, row, leftColumn, usedPieces)
	}

	// right
	// row same, column + 1
	rightColumn := column + 1
	if rightColumn <= c.board.numRowsCols-1 && !pieceUsed(c.board.pieces[row][rightColumn].id, usedPieces) {
		c.solveBoggle(currentWord, row, rightColumn, usedPieces)
	}

	// upper right
	// row - 1, column + 1
	upRightRow := row - 1
	upRightColumn := column + 1
	if upRightRow >= 0 && upRightColumn <= c.board.numRowsCols-1 && !pieceUsed(c.board.pieces[upRightRow][upRightColumn].id, usedPieces) {
		c.solveBoggle(currentWord, upRightRow, upRightColumn, usedPieces)
	}

	// lower right
	// row + 1, column + 1
	lowerRightRow := row + 1
	lowerRightColumn := column + 1
	if lowerRightRow <= c.board.numRowsCols-1 && lowerRightColumn <= c.board.numRowsCols-1 && !pieceUsed(c.board.pieces[lowerRightRow][lowerRightColumn].id, usedPieces) {
		c.solveBoggle(currentWord, lowerRightRow, lowerRightColumn, usedPieces)
	}

	// upper left
	// row - 1, column - 1
	upLeftRow := row - 1
	upLeftColumn := column - 1
	if upLeftRow >= 0 && upLeftColumn >= 0 && !pieceUsed(c.board.pieces[upLeftRow][upLeftColumn].id, usedPieces) {
		c.solveBoggle(currentWord, upLeftRow, upLeftColumn, usedPieces)
	}

	// lower left
	// row + 1, column - 1
	lowerLeftRow := row + 1
	lowerLeftColumn := column - 1
	if lowerLeftRow <= c.board.numRowsCols-1 && lowerLeftColumn >= 0 && !pieceUsed(c.board.pieces[lowerLeftRow][lowerLeftColumn].id, usedPieces) {
		c.solveBoggle(currentWord, lowerLeftRow, lowerLeftColumn, usedPieces)
	}

	return
}

// Safely add answer to answers
// adding to map removes the possiblity of adding duplicates
func (s *Solve) addAnswer(ans string) {
	s.answerLock.Lock()
	defer s.answerLock.Unlock()
	s.answers[ans] = ans
}

// retrieve the list of answers for the board
func (s *Solve) getAnswers() []string {
	s.answerLock.Lock()
	defer s.answerLock.Unlock()
	answers := []string{}
	for _, value := range s.answers {
		answers = append(answers, value)
	}

	return answers
}
