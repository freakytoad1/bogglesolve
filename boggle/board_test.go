package boggle

import (
	"reflect"
	"testing"
)

func Test_createBoard(t *testing.T) {
	tests := []struct {
		name       string
		inputBoard []string
		want       *board
	}{
		{
			name:       "POSITIVE - good board",
			inputBoard: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"},
			want: &board{
				pieces: [][]piece{
					{piece{"a", 0}, piece{"b", 1}, piece{"c", 2}, piece{"d", 3}},
					{piece{"e", 4}, piece{"f", 5}, piece{"g", 6}, piece{"h", 7}},
					{piece{"i", 8}, piece{"j", 9}, piece{"k", 10}, piece{"l", 11}},
					{piece{"m", 12}, piece{"n", 13}, piece{"o", 14}, piece{"p", 15}},
				},
				numRowsCols: 4,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createBoard(tt.inputBoard); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("createBoard() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_pieceUsed(t *testing.T) {
	tests := []struct {
		name       string
		pieceID    int
		usedPieces []int
		want       bool
	}{
		{
			name:       "POSITIVE - the piece is used",
			pieceID:    1,
			usedPieces: []int{2, 3, 4, 1},
			want:       true,
		},
		{
			name:       "NEGATIVE - the piece is unused",
			pieceID:    1,
			usedPieces: []int{2, 3, 4},
			want:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pieceUsed(tt.pieceID, tt.usedPieces); got != tt.want {
				t.Errorf("pieceUsed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_newBoard(t *testing.T) {
	tests := []struct {
		name       string
		inputBoard string
		want       [4][4]piece
		wantErr    bool
	}{
		{
			name:       "POSITIVE - good board",
			inputBoard: "abcdefghijklmnop",
			want: [4][4]piece{
				{piece{"a", 0}, piece{"b", 1}, piece{"c", 2}, piece{"d", 3}},
				{piece{"e", 4}, piece{"f", 5}, piece{"g", 6}, piece{"h", 7}},
				{piece{"i", 8}, piece{"j", 9}, piece{"k", 10}, piece{"l", 11}},
				{piece{"m", 12}, piece{"n", 13}, piece{"o", 14}, piece{"p", 15}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newBoard(tt.inputBoard)
			if (err != nil) != tt.wantErr {
				t.Errorf("newBoard() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newBoard() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_validateInputBoard(t *testing.T) {
	tests := []struct {
		name       string
		characters string
		want       []string
		wantErr    bool
	}{
		{
			name:       "POSITIVE - valid board",
			characters: "abcdefghijklmnop",
			want:       []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"},
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := validateInputBoard(tt.characters)
			if (err != nil) != tt.wantErr {
				t.Errorf("validateInputBoard() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("validateInputBoard() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isSq(t *testing.T) {
	tests := []struct {
		name string
		n    float64
		want bool
	}{
		{
			name: "POSITIVE - is sq rt",
			n:    25,
			want: true,
		},
		{
			name: "NEGATIVE - not sq rt",
			n:    26,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isSq(tt.n); got != tt.want {
				t.Errorf("isSq() = %v, want %v", got, tt.want)
			}
		})
	}
}
