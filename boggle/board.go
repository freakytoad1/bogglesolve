package boggle

import (
	"fmt"
	"math"
	"strings"
	"unicode"
)

type board struct {
	pieces      [][]piece // contains the board pieces
	numRowsCols int       // contains the number of rows in board (and columns, but they will be the same #)
}

type piece struct {
	character string // the actual letter
	id        int    // id of the letter. important if there are multiple of the same letter
}

// newBoard turns the characters passed in into a 'board'.
func newBoard(inputBoard string) (*board, error) {
	// Validate characters
	validatedBoard, err := validateInputBoard(inputBoard)
	if err != nil {
		return nil, err
	}

	// Create Board
	goodBoard := createBoard(validatedBoard)

	return goodBoard, nil
}

// createBoard transforms an array of string chars to a 4x4 game board
func createBoard(inputBoard []string) *board {
	n := int(math.Sqrt(float64(len(inputBoard))))
	pieces := make([][]piece, n)

	// initialize each spot in board
	for i := range pieces {
		pieces[i] = make([]piece, n)
	}

	// place each letter from input into a spot on the board and give it an ID
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			spot := i*n + j
			pieces[i][j] = piece{
				character: inputBoard[spot],
				id:        spot,
			}
		}
	}

	return &board{
		pieces:      pieces,
		numRowsCols: n,
	}
}

// validateInputBoard will check that each character is a letter,
// the correct # of characters was provided, and turn all characters into lowercase.
// NOTE: expected input here is a string with a len that is a square
func validateInputBoard(characters string) ([]string, error) {
	// determine if input is a square, board has to be square
	inputLength := float64(len(characters))
	if !isSq(inputLength) {
		return nil, fmt.Errorf("input board was not square")
	}

	validatedBoard := []string{}

	// check that each char is a letter
	for _, character := range characters {
		if !unicode.IsLetter(character) {
			return nil, fmt.Errorf("provided character [%s] is not a letter", string(character))
		}

		// add to return
		validatedBoard = append(validatedBoard, strings.ToLower(string(character)))
	}

	// return a lowercase version of the input
	return validatedBoard, nil
}

func isSq(n float64) bool {
	sqrt := math.Sqrt(n)
	flooredSqrt := math.Floor(sqrt)
	if n != flooredSqrt*flooredSqrt {
		return false
	}

	return true
}
