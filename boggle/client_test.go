package boggle

import (
	"net/http"
	"reflect"
	"testing"

	"github.com/jarcoal/httpmock"
	"gitlab.com/freakytoad1/bogglesolve/trie"
)

func Test_isAlphabetic(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want bool
	}{
		{
			name: "POSITIVE - string is alphabetic",
			s:    "toad",
			want: true,
		},
		{
			name: "NEGATIVE - string contains #",
			s:    "toad1",
			want: false,
		},
		{
			name: "NEGATIVE - empty",
			s:    "",
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isAlphabetic(tt.s); got != tt.want {
				t.Errorf("isAlphabetic() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_retrieveOnlineWordlist(t *testing.T) {
	// mock the response for site
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		http.MethodGet,
		"https://www-personal.umich.edu/~jlawler/wordlist",
		httpmock.NewStringResponder(
			http.StatusOK,
			"alligator\r\nbulldog\r\ncat\r\n",
		),
	)

	wordlist := make(map[string]struct{})
	wordlist["alligator"] = struct{}{}
	wordlist["bulldog"] = struct{}{}
	wordlist["cat"] = struct{}{}

	tests := []struct {
		name    string
		want    map[string]struct{}
		wantErr bool
	}{
		{
			name:    "POSITIVE - gets the wordlist",
			want:    wordlist,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := retrieveOnlineWordlist()
			if (err != nil) != tt.wantErr {
				t.Errorf("retrieveOnlineWordlist() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("retrieveOnlineWordlist() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	// mock the response for site
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		http.MethodGet,
		"https://www-personal.umich.edu/~jlawler/wordlist",
		httpmock.NewStringResponder(
			http.StatusOK,
			"alligator\r\nbulldog\r\ncat\r\n",
		),
	)

	wordlist := trie.New()
	wordlist.Insert("alligator")
	wordlist.Insert("bulldog")
	wordlist.Insert("cat")

	tests := []struct {
		name    string
		want    *Client
		wantErr bool
	}{
		{
			name: "POSITIVE - creates Client with wordlist",
			want: &Client{
				WordList: wordlist,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New()
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
