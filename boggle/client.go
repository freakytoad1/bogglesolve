package boggle

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"unicode"

	"gitlab.com/freakytoad1/bogglesolve/trie"
)

type Client struct {
	WordList *trie.Trie
}

// New creates the Client object and retrieves the word list from online
func New() (*Client, error) {
	// Retrieve wordlist
	wordlist, err := retrieveOnlineWordlist()
	if err != nil {
		return nil, err
	}

	return &Client{
		WordList: wordlist,
	}, nil
}

func retrieveOnlineWordlist() (*trie.Trie, error) {
	// this is a very rudimentary GET request
	resp, err := http.Get("https://www-personal.umich.edu/~jlawler/wordlist")
	if err != nil {
		return nil, fmt.Errorf("unable to retrieve online wordlist: %w", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to read in online wordlist: %w", err)
	}

	// split newlines into new words
	// convert to map, way faster to search a map that traverse a slice
	wordlist := trie.New()
	for _, word := range strings.Split(string(body), "\r\n") {
		// Rule 1. Words must be at least three letters in length.
		if !isAlphabetic(word) || len(word) < 3 {
			// skip the bad words in list
			continue
		}

		// use empty struct as the value since we only care about the key
		wordlist.Insert(word)
	}

	// log.Printf("Wordlist length: %d", len(wordlist))

	return wordlist, nil
}

// determines if word is only letters
func isAlphabetic(s string) bool {
	if s == "" {
		return false // empty string is not alpha
	}

	for _, r := range s {
		if !unicode.IsLetter(r) {
			return false
		}
	}
	return true
}
