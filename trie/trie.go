package trie

import "reflect"

type Answer struct {
	Match       bool
	FutureMatch bool
}

type node struct {
	children map[rune]*node
	isWord   bool
}

type Trie struct {
	root *node
}

func New() *Trie {
	return &Trie{
		root: &node{
			children: make(map[rune]*node),
		},
	}
}

// Inserts add a new word to the Trie
func (c *Trie) Insert(w string) {
	// loop over input word to add to Trie
	currentNode := c.root
	for _, char := range w {
		if _, exists := currentNode.children[char]; !exists {
			currentNode.children[char] = &node{
				children: make(map[rune]*node),
			}
		}

		currentNode = currentNode.children[char]
	}

	currentNode.isWord = true
}

// Search sees if a word is in the trie or is the start of a larger word in trie
func (c *Trie) Search(w string) Answer {
	response := Answer{}
	// loop over input word to see if it is in Trie
	currentNode := c.root
	for _, char := range w {
		if _, exists := currentNode.children[char]; !exists {
			// not matching, not a word or future word
			return response
		}

		currentNode = currentNode.children[char]
	}

	// if this current node is a word, we match
	if currentNode.isWord {
		response.Match = true
	}

	// is future word? does the currentNode have children
	emptyChildren := make(map[rune]*node)
	if !reflect.DeepEqual(currentNode.children, emptyChildren) {
		response.FutureMatch = true
	}

	return response
}
