package trie

import (
	"reflect"
	"testing"
)

func TestTrie_Insert(t *testing.T) {
	type fields struct {
		root *node
	}

	gChild := make(map[rune]*node)
	gChild['g'] = &node{
		children: make(map[rune]*node),
		isWord:   true,
	}

	eChild := make(map[rune]*node)
	eChild['e'] = &node{
		children: gChild,
		isWord:   false,
	}

	mChild := make(map[rune]*node)
	mChild['m'] = &node{
		children: eChild,
		isWord:   false,
	}

	tests := []struct {
		name   string
		fields fields
		w      string
		want   *node
	}{
		{
			name: "POSITIVE - adds word to trie",
			fields: fields{
				root: &node{
					children: make(map[rune]*node),
				},
			},
			w: "meg",
			want: &node{
				children: mChild,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Trie{
				root: tt.fields.root,
			}
			c.Insert(tt.w)

			got := c.root

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Insert() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrie_Search(t *testing.T) {
	type fields struct {
		root *node
	}

	test1 := New()
	test1.Insert("meg")
	test1.Insert("me")
	test1.Insert("men")
	test1.Insert("mehg")

	tests := []struct {
		name   string
		fields fields
		w      string
		want   Answer
	}{
		{
			name: "POSITIVE - finds word in Trie",
			fields: fields{
				root: test1.root,
			},
			w: "meg",
			want: Answer{
				Match:       true,
				FutureMatch: false,
			},
		},
		{
			name: "POSITIVE - finds word and future in Trie",
			fields: fields{
				root: test1.root,
			},
			w: "me",
			want: Answer{
				Match:       true,
				FutureMatch: true,
			},
		},
		{
			name: "POSITIVE - finds word and future in Trie",
			fields: fields{
				root: test1.root,
			},
			w: "men",
			want: Answer{
				Match:       true,
				FutureMatch: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Trie{
				root: tt.fields.root,
			}
			if got := c.Search(tt.w); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Trie.Search() = %v, want %v", got, tt.want)
			}
		})
	}
}
