package main

import (
	"log"

	"gitlab.com/freakytoad1/bogglesolve/server"
)

func main() {
	// start api server on localhost:8080
	// expose the /boggle endpoint to solve boards
	if err := server.Start(); err != nil {
		log.Fatalf("unable to start boggle server: %v", err)
	}
}
